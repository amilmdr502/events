<?php

namespace App\Http\Controllers;

use App\Models\EventCategories;
use App\Models\Events;
use App\Models\EventTag;
use App\Models\Tags;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->uploadpath = public_path("/images/events");
        if (!File::exists($this->uploadpath)) {
            File::makeDirectory($this->uploadpath, 0777, true, true);
        }
}
    public function index(Request $request)
    {

        if($request->ajax()){
            $data = Events::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $actionBtn = '
                <a class="btn btn-sm btn-success edit-item-btn"
                    href="' . route('events.edit', $row->id) . '">Edit</a>
                    <button type="submit" id="'.$row->id.'"
                        class="btn btn-sm btn-danger remove-item-btn delete"
                        data-toggle="tooltip" title="Delete">Delete</button>
                ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('backend.events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories =EventCategories::all();
        $tags = Tags::all();
        return view('backend.events.create',compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "event_title" => "required|unique:events,event_title",
            "event_status"=>"required",
            "event_image"=>'image|mimes:png,jpg,jpeg,gif',
            "event_description"=>"required",
            "category_id"=>"required",
        ]);
         try {
            $events =  new Events();
            $events->event_title=$request->input('event_title');
            $events->event_status=$request->input('event_status');

            if ($request->hasfile('event_image')) {
                $pages_image = $request->file('event_image');
                $filename = time() . "." . $pages_image->getClientOriginalExtension();
                $pages_image->move($this->uploadpath, $filename);
                $events->event_image=$filename;
            }
            $events->event_description=$request->input('event_description');
            $events->category_id=$request->input('category_id');
            $events->save();
                    if (!empty($request["tags"])) {
                foreach ($request["tags"] as $t) {
                    $tag = new EventTag();
                    $tag->event_id = $events->id;
                    $tag->tag_id = $t;
                    $tag->save();
                }
            }
            request()->session()->flash("successMessage", "Events added successfully");
        }
        catch (\Exception $e) {
            request()->session()->flash('unsuccessMessage', 'Failed to add Events');
        }
        return redirect(route('events.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function show(Events $events)
    {



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $events = Events::find($id);
        $event_tag = EventTag::where("event_id", $id)->get('tag_id');
        $edit_tags = [];
        foreach($event_tag as $item){
            $edit_tags[] = $item->tag_id;
        }
        // dd($edit_tags);
        $tags = Tags::all();
        $categories =EventCategories::all();
        return view('backend.events.edit',compact('events','edit_tags','tags','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {


        $request->validate([
            "event_title" => "required|unique:events,event_title,".$id,
            "event_status"=>"required",
            "event_image"=>'image|mimes:png,jpg,jpeg,gif',
            "event_description"=>"required",
            "category_id"=>"required",
        ]);
         try {
            $events = Events::where("id", $id)->first();
            $events->event_title=$request->input('event_title');
            $events->event_status=$request->input('event_status');

            if ($request->hasfile('event_image')) {
                $image = $events->event_image;
                $pages_image = $request->file('event_image');
                $filename = time() . "." . $pages_image->getClientOriginalExtension();
                $pages_image->move($this->uploadpath, $filename);
                $events->event_image=$filename;
                File::delete($this->uploadpath . '/' . $image);
            }
            else{
                $events->event_image = $events['event_image'];
            }
            $events->event_description=$request->input('event_description');
            $events->category_id=$request->input('category_id');
            $events->save();
            EventTag::where("event_id", $events->id)->delete();
                    if (!empty($request["tags"])) {
                foreach ($request["tags"] as $t) {
                    $tag = new EventTag();
                    $tag->event_id = $events->id;
                    $tag->tag_id = $t;
                    $tag->save();
                }
            }
            request()->session()->flash("successMessage", "Events update successfully");
        }
        catch (\Exception $e) {
            request()->session()->flash('unsuccessMessage', 'Failed to update Events');
        }
        return redirect(route('events.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            EventTag::where("event_id", $id)->delete();
            Events::destroy($id);
        }
        catch(\Exception $e){
            return response()->json(["status" => "Event deleted Fail"]);
        }
        return response()->json(["status" => "Event deleted successfully"]);

    }
}
