<?php

namespace App\Http\Controllers;

use App\Models\Tags;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Tags::select('*');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $actionBtn = '
                <a class="btn btn-sm btn-success edit-item-btn"
                    href="' . route('tags.edit', $row->id) . '">Edit</a>
                    <button type="submit" id="'. $row->id.'"
                        class="btn btn-sm btn-danger remove-item-btn delete"
                        data-toggle="tooltip" title="Delete">Delete</button>
                ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('backend.tags.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "tags_title" => "required|unique:tags,tags_title",
            "tags_status" => "required",
        ]);
        try {
            $input = $request->all();
            Tags::create($input);
            request()->session()->flash("successMessage", "Tags added successfully");
        }
        catch (\Exception $e) {
            request()->session()->flash('unsuccessMessage', 'Failed to add Tags');
        }
        return redirect(route('tags.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(Tags $tags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = Tags::find($id);
        return view('backend.tags.edit',compact('tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            "tags_title" => "required|unique:tags,tags_title,".$id,
            "tags_status" => "required",
        ]);
        try{
            $tags = Tags::find($id);
            $tags->tags_title = $request->input('tags_title');
            $tags->tags_status = $request->input('tags_status');
            $tags->Update();
            request()->session()->flash("successMessage", "Tags Update successfully");


        }
        catch(\Exception $e){
            request()->session()->flash('unsuccessMessage', 'Failed to update Tags');

        }


        return redirect(route('tags.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function destroy($tags)
    {
        try{
            // Tags::deleted($tags);
            request()->session()->flash("successMessage", "Tags Delete successfully");
        }
        catch(\Exception $e){
            request()->session()->flash('unsuccessMessage', 'Failed to delete Tags');
        }
        return redirect(route('tags.index'));
    }
}
