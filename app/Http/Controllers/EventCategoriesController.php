<?php

namespace App\Http\Controllers;

use App\Models\EventCategories;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class EventCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        if($request->ajax()){
            $data =EventCategories::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $actionBtn = '
                <a class="btn btn-sm btn-success edit-item-btn"
                    href="' . route('categorie.edit', $row->id) . '">Edit</a>
                    <button type="submit" id="'. $row->id.'"
                        class="btn btn-sm btn-danger remove-item-btn delete"
                        data-toggle="tooltip" title="Delete">Delete</button>
                ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('backend.event_categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.event_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "event_categorie_title" => "required|unique:event_categories,event_categorie_title",
            "event_categorie_status" => "required",
        ]);
        try {
            $input = $request->all();
            // dd($input);
            EventCategories::create($input);
            request()->session()->flash("successMessage", "Categories added successfully");
        }
        catch (\Exception $e) {
            request()->session()->flash('unsuccessMessage', 'Failed to add Categories');
        }
        return redirect(route('categorie.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventCategories  $eventCategories
     * @return \Illuminate\Http\Response
     */
    public function show(EventCategories $eventCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventCategories  $eventCategories
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $categories =EventCategories::find($id);
        return view('backend.event_categories.edit',compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventCategories  $eventCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            "event_categorie_title" => "required|unique:tags,tags_title,".$id,
            "event_categorie_status" => "required",
        ]);
        try{
            $categories = EventCategories::find($id);
            $categories->event_categorie_title = $request->input('event_categorie_title');
            $categories->event_categorie_status = $request->input('event_categorie_status');
            $categories->Update();
            request()->session()->flash("successMessage", "Tags Update successfully");


        }
        catch(\Exception $e){
            request()->session()->flash('unsuccessMessage', 'Failed to update Tags');

        }
        return redirect(route('categorie.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventCategories  $eventCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($eventCategories)
    {
        try{
            EventCategories::deleted($eventCategories);
            request()->session()->flash("successMessage", "Tags Delete successfully");
        }
        catch(\Exception $e){
            request()->session()->flash('unsuccessMessage', 'Failed to delete Tags');
        }
        return redirect(route('categorie.index'));
    }
}
