<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    use HasFactory;
    protected $table = "tags";

    protected $fillable = [
    	"tags_title",
        "tags_status"
    ];
    public function events()
    {
        return $this->belongsToMany(Events::class);
    }





}
