<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Events extends Model
{
    use Sluggable;
    use HasFactory;
    protected $table = "events";

    protected $fillable = [
    	"event_title",
        "event_slug",
        "event_status",
        "event_image",
        "event_description",
        "category_id",
    ];

    public function sluggable(): array
    {
        return [
            'event_slug' => [
                'source' => 'event_title'
            ]
        ];
    }
    public function tags()
    {
        return $this->belongsToMany(Tags::class);
    }




}
