<?php

use App\Http\Controllers\EventCategoriesController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\TagsController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('backend.layouts.master');
    });
    Route::get('home', function () {
        return view('backend.layouts.master');
    });
    Route::resource('events', EventsController::class);
    Route::resource('tags', TagsController::class);
    Route::resource('categorie', EventCategoriesController::class);
});
