<!-- jQuery -->
<script src="{{ asset('backend/layout/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('backend/layout/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 4 -->
<script src="{{ asset('backend/layout/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('backend/layout/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('backend/layout/plugins/sparklines/sparkline.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('backend/layout/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('backend/layout/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('backend/layout/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('backend/layout/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}">
</script>
<!-- Summernote -->
<script src="{{ asset('backend/layout/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('backend/layout/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/layout/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('backend/layout/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('backend/layout/dist/js/demo.js') }}"></script>
<script src="{{ asset('backend/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
{{--  <script src="{{ asset('backend/select2/js/select2.full.min.js') }}" type="text/javascript"></script>  --}}
<script src="{{ asset('backend/sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/dataTable/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<script>
    CKEDITOR.replace("description");
    $(document).ready(function(){

        var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
           removeItemButton: true,
           maxItemCount:5,
           searchResultLimit:5,
           renderChoiceLimit:5
         });


    });
</script>
