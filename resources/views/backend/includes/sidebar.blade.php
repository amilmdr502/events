<aside class="main-sidebar sidebar-dark-primary elevation-4">
   {{--  <a href="{{ route('frontend.home') }}" class="brand-link" target="_blank">  --}}
      <span class="text-uppercase text-primary "  style="width:8rem;">{{ env('APP_NAME') }}</span>
   {{--  </a>  --}}
   <div class="sidebar">
      <nav class="mt-2">
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
               <a href="{{ route('events.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-ad nav-icon"></i>
                  <p>
                     Events

                  </p>
               </a>
            </li>
            <li class="nav-item ">
                <a href="{{ route('tags.index') }}" class="nav-link">
                   <i class="nav-icon fas fa-ad nav-icon"></i>
                   <p>
                      Tags

                   </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('categorie.index') }}" class="nav-link">
                   <i class="nav-icon fas fa-ad nav-icon"></i>
                   <p>
                      Categorys
                   </p>
                </a>
            </li>


            <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav-link"  onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
   {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
         </ul>
      </nav>
      <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
</aside>
