<link rel="icon" href="{{ asset('frontend/images/logo.png') }}" type="image/gif" sizes="32*32">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
{{--  <meta name="_token" content="{{ csrf_token() }}">  --}}
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>@yield('title')</title>
<link rel="icon" href="{{ asset('frontend/images/default-big-image.png') }}">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('backend/layout/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">

<!-- iCheck -->

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

<!-- JQVMap -->

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/jqvmap/jqvmap.min.css') }}">

<!-- Theme style -->

<link rel="stylesheet" href="{{ asset('backend/layout/dist/css/adminlte.min.css') }}">

<!-- overlayScrollbars -->

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

<!-- Daterange picker -->

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/daterangepicker/daterangepicker.css') }}">

<!-- summernote -->

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/summernote/summernote-bs4.css') }}">

<!-- Google Font: Source Sans Pro -->

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
{{--  <link rel="stylesheet" href="{{ asset('backend/dataTable/jquery.dataTables.min.css')}}">  --}}

<link rel="stylesheet" href="{{ asset('backend/select2/css/select2.min.css')}}">
{{--  <link rel="stylesheet" href="{{ asset('backend/select2-bootstrap4-theme/css/select2-bootstrap4.min.css') }}">  --}}

<link rel="stylesheet" href="{{ asset('backend/layout/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('backend/custom.css') }}">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<style>
    .mt-100{margin-top: 100px}
    body{background: #00B4DB;
        background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
        background: linear-gradient(to right, #0083B0, #00B4DB);
        color: #514B64;
        min-height: 100vh}
</style>
