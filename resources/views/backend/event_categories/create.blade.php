@extends('backend.layouts.master')
@section('title', env('APP_NAME') . ' | Add Categories')
@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Categories</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('categorie.index') }}">Categories</a></li>
                        <li class="breadcrumb-item active">Add Categories</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <form action="{{ route('categorie.store') }}" method="POST" id="news_store" enctype="multipart/form-data" autocomplete="off">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Categories</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label for="event_categorie_title">Title</label>
                                    <input type="text" class="form-control" name="event_categorie_title" id="event_categorie_title"
                                        placeholder="Title" value="{{ old('event_categorie_title') }}">
                                        @if ($errors->has('event_categorie_title'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('event_categorie_title') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 5px;">
                                            <input type="radio" name="event_categorie_status" value="1"
                                                {{ !is_null(old('event_categorie_status')) ? (old('event_categorie_status') == 1 ? 'checked' : '') : '' }}>
                                            <span>Active</span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="radio" name="event_categorie_status" value="0"
                                                {{ !is_null(old('event_categorie_status')) ? (old('event_categorie_status') == 0 ? 'checked' : '') : '' }}>
                                            <span>Inactive</span><br>
                                        </div>
                                    </div>
                                    @if ($errors->has('event_categorie_status'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('event_categorie_status') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="col-12" style="margin-bottom: 10px;">
                           <input type="submit" value="Submit" class="btn btn-sm btn-primary float-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
