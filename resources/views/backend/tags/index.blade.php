@extends('backend.layouts.master')
@section('title', env('APP_NAME') . ' | List News')
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            Tags
                        </li>
                        <li class="breadcrumb-item active">
                            List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title"><strong>List Tags</strong></h3>
                    <div class="card-tools" style="display: flex;">
                        <div style="margin-right: 10px;">
                            <a href="{{ route('tags.create') }}" class="btn btn-success btn-xs" title="Add Tags">Add
                                Tags</a>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive no-padding">
                    <div my-2>
                        @if (session('successMessage'))
                            <div class="alert alert-success"  id="message_id" role="alert">
                                {{ session('successMessage') }}
                            </div>
                            @elseif (session('unsuccessMessage'))
                            <div class="alert alert-danger"  id="message_id" role="alert">
                                {{ session('unsuccessMessage') }}
                            </div>
                        @endif
                    </div>
                    <table class="table table-hover" id="myTable">

                        <thead>

                            <tr>
                                <th width="5%">S.N</th>

                                <th width="33%">Title</th>

                                <th width="8%">Status</th>

                                <th width="15%">Action</th>

                            </tr>

                        </thead>

                    </table>

                </div>

            </div>

            <!-- /.box -->

        </div>

    </div>

@endsection
@section('after-script')

<script type="text/javascript">
    $(function () {

      var table = $('#myTable').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{ route('tags.index') }}",
          columns: [
              {data: 'id', name: 'id'},
              {data: 'tags_title', name: 'tags_title'},
              {data: 'tags_status', name: 'tags_status'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
      $("body").on('click', '.delete', function (e){
        e.preventDefault();
        var deleteId = $(this).data("id");
        var url = '{{ url('/') }}';
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this Tags!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) {
            $.ajax({
              type: "delete",
              url: url +'/tags/'+ deleteId,
              data: data,
              success: function (response){
                swal(response.status, {
                  icon: "success",
                }).then((result) => {
                  location.reload();
                });
              },
            });
          } else {
            swal("Your Tags is safe!");
          }
        });
      });

    });
  </script>
@endsection
