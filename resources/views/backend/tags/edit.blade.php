@extends('backend.layouts.master')
@section('title', env('APP_NAME') . ' | Edit news')
@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit News</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('tags.index') }}">News</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <form class="form-horizontal" method="post" action="{{ route('tags.update',$tags->id)}}"
        id="news_edit" enctype="multipart/form-data" autocomplete="off">
        @csrf
        @method('patch')
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Tags</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label for="tags_title">Title</label>
                                    <input type="text" class="form-control" name="tags_title" id="tags_title"
                                        placeholder="Title" value="{{ !empty($tags) ? $tags->tags_title : '' }}">
                                        @if ($errors->has('tags_title'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('tags_title') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 5px;">
                                            <input type="radio" name="tags_status" value="1"
                                    <input type="radio" name="status" value="1" {{ !is_null($tags) ? (($tags->tags_status==1)? 'checked' : '') : '' }}>
                                            <span>Active</span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="radio" name="tags_status" value="0"
                                            {{ !is_null($tags) ? (($tags->tags_status==0)? 'checked' : '') : '' }}>
                                            <span>Inactive</span><br>
                                        </div>
                                    </div>
                                    @if ($errors->has('tags_status'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('tags_status') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="col-12" style="margin-bottom: 10px;">
                            <input type="submit" value="Update" class="btn btn-sm btn-primary float-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('after-script')
@endsection
