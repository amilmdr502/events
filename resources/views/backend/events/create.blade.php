@extends('backend.layouts.master')
@section('title', env('APP_NAME') . ' | Add Events')
@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Event</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('events.index') }}">Events</a></li>
                        <li class="breadcrumb-item active">Add Event</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
@section('content')
    <form action="{{ route('events.store') }}" method="POST" id="news_store" enctype="multipart/form-data" autocomplete="off">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Event</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label for="event_title">Title</label>
                                    <input type="text" class="form-control" name="event_title" id="title"
                                        placeholder="Title" value="{{ old('event_title') }}">
                                        @if ($errors->has('event_title'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('event_title') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Events Image</label>
                                    <img class="card-img-top" src="{{ asset('default.jpg') }}" alt="" height="200px"
                                        width="200px" id="pic">

                                </div>
                            </div>
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <input type="file" name="event_image"
                                        onchange="pic.src=window.URL.createObjectURL(this.files[0])" accept="image/*">
                                    </p>
                                </div>
                                @if ($errors->has('event_image'))
                                <span class="help-block" style="color: #f86c6b;">
                                    {{ $errors->first('event_image') }}
                                </span>
                            @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 5px;">
                                            <input type="radio" name="event_status" value="1"
                                                {{ !is_null(old('event_status')) ? (old('event_status') == 1 ? 'checked' : '') : '' }}>
                                            <span>Publish</span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="radio" name="event_status" value="0"
                                                {{ !is_null(old('event_status')) ? (old('event_status') == 0 ? 'checked' : '') : '' }}>
                                            <span>Unpublish</span><br>
                                        </div>
                                    </div>
                                    @if ($errors->has('event_status'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('event_status') }}
                                        </span>
                                    @endif
                                </div>
                            </div>



                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label>Tags</label>
                                    <div class="row d-flex justify-content-left">
                                        <div class="col-md-6"> <select id="choices-multiple-remove-button"
                                                name="tags[]" placeholder="Select upto 5 tags" multiple>
                                                @foreach ($tags as $items)
                                                    <option value="{{ $items->id }}"
                                                        {{ !empty(old('items_id')) ? (in_array($items->id, old('items_id')) ? 'selected' : '') : '' }}>
                                                        {{ $items->tags_title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('tags.*'))
                                                <span class="help-block" style="color: #f86c6b;">
                                                    {{ $errors->first('tags.*') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-6 mb-10">
                                <div class="form-group">
                            <label for="category_id">Choose a Categories</label>

                            <select name="category_id" class="form-control" id="cars">
                                @foreach ($categories as $items)
                                <option value="{{ $items->id }}"
                                    {{ !empty(old('items_id')) ? (in_array($items->id, old('items_id')) ? 'selected' : '') : '' }}>
                                    {{ $items->event_categorie_title }}</option>
                            @endforeach
                            </select>
                            @if ($errors->has('tags.*'))
                            <span class="help-block" style="color: #f86c6b;">
                                {{ $errors->first('tags.*') }}
                            </span>
                        @endif
                                </div>
                            </div>







                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label for="event_description">Description</label>
                                    <textarea type="text" class="form-control editor" name="event_description" id="description">{{ old('event_description') }}</textarea>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="col-12" style="margin-bottom: 10px;">
                            <input type="submit" value="Submit" class="btn btn-sm btn-primary float-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
