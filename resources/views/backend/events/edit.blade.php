@extends('backend.layouts.master')
@section('title', env('APP_NAME') . ' | Edit Events')
@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Events</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('events.index') }}">Events</a></li>
                        <li class="breadcrumb-item active">Edit</li>

                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
@section('content')

{{--  {{  dd($events) }}  --}}
    <form class="form-horizontal" method="post" action="{{ route('events.update', $events->id) }}"
        id="news_edit"enctype="multipart/form-data" autocomplete="off">
        @csrf
        @method('patch')
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Events</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label for="event_title">Title</label>
                                    <input type="text" class="form-control" name="event_title" id="title"
                                        placeholder="Title"  value="{{ !empty($events) ? $events->event_title : '' }}">
                                        @if ($errors->has('event_title'))
                                        <span class="help-block" style="color: #f86c6b;">
                                            {{ $errors->first('event_title') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Events Image</label>
                                    <img class="card-img-top" src="{{ (($events->event_image != '') && file_exists(public_path('images/events/'.$events->event_image))) ? asset('images/events/'.$events->event_image) : asset('default.jpg') }}"  alt="" height="200px"
                                        width="200px" id="pic">

                                </div>
                            </div>
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <input type="file" name="event_image"
                                        onchange="pic.src=window.URL.createObjectURL(this.files[0])" accept="image/*">
                                    </p>
                                </div>
                                @if ($errors->has('event_image'))
                                <span class="help-block" style="color: #f86c6b;">
                                    {{ $errors->first('event_image') }}
                                </span>
                            @endif
                            </div>
                            <div class="form-group">
                                <label>Status</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 5px;">
                                        <input type="radio" name="event_status" value="1"
                                <input type="radio" name="status" value="1" {{ !is_null($events) ? (($events->event_status==1)? 'checked' : '') : '' }}>
                                        <span>Active</span>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="radio" name="event_status" value="0"
                                        {{ !is_null($events) ? (($events->event_status==0)? 'checked' : '') : '' }}>
                                        <span>Inactive</span><br>
                                    </div>
                                </div>
                                @if ($errors->has('event_status'))
                                    <span class="help-block" style="color: #f86c6b;">
                                        {{ $errors->first('event_status') }}
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label>Tags</label>
                                    <div class="row d-flex justify-content-left">
                                        <div class="col-md-6"> <select id="choices-multiple-remove-button"
                                                name="tags[]" placeholder="Select upto 5 tags" multiple>
                                                @foreach ($tags as $items)
                                                    <option value="{{ $items->id }}"
                                                        {{ !empty($events)? (in_array($items->id, $edit_tags)? 'selected' : '') : '' }}>{{ $items->tags_title}}</option>
                                                        {{ $items->tags_title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('tags.*'))
                                                <span class="help-block" style="color: #f86c6b;">
                                                    {{ $errors->first('tags.*') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-6 mb-10">
                                <div class="form-group">
                            <label for="category_id">Choose a Categories</label>

                            <select name="category_id" class="form-control">
                                @foreach ($categories as $items)
                                <option value="{{ $items->id }}"
                                   @if($items->id == $events->category_id)  selected @endif >
                                    {{ $items->event_categorie_title }}</option>
                            @endforeach
                            </select>
                            @if ($errors->has('tags.*'))
                            <span class="help-block" style="color: #f86c6b;">
                                {{ $errors->first('tags.*') }}
                            </span>
                        @endif
                                </div>
                            </div>
                            <div class="col-md-12 mb-10">
                                <div class="form-group">
                                    <label for="event_description">Description</label>
                                    <textarea type="text" class="form-control editor" name="event_description" id="description">{{ ($events->event_description) }}</textarea>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="col-12" style="margin-bottom: 10px;">
                            <input type="submit" value="Update" class="btn btn-sm btn-primary float-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

